Task 2 solution

In this application I use Spring Boot to configure dependencies - this is an overkill for a small test app,
but my goal is to demonstrate here that I have experience with Spring Boot and testing (or have excellent googling skills:) ).

To run the app:
mvn spring-boot:run