package com.slava.upwork.service;

import org.springframework.stereotype.Service;

@Service
public class PhraseParserService {

    /**
     * Parse and sanitize input
     *
     * @param phrase the phrase to parse
     * @return array of words
     */
    public String[] parseWords(String phrase) {
        if (phrase == null || phrase.isEmpty()) {
            throw new IllegalArgumentException("Phrase is blank");
        }

        return phrase.split("\\s+");
    }
}
