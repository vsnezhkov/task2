package com.slava.upwork.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class WordFilterService {

    @Autowired
    private PhraseParserService phraseParserService;

    /**
     * There is a method for this in Java 11
     */
    private String repeat(String word, int count) {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < count; i++) {
            sb.append(word);

            if (i < count - 1) {
                sb.append(" ");
            }
        }

        return sb.toString();
    }

    /**
     * The good thing about this approach is that it is O(n)
     * <p>
     * Probably more readable solutions using Java 8 streams/reducing, not sure though.
     * <p>
     * I tried to return exactly the same string as in test example, it complicated things a little.
     */
    public String getSequencesString(String phrase) {

        String[] words = phraseParserService.parseWords(phrase);

        StringBuilder result = new StringBuilder();

        for (int i = 0; i < words.length - 1; i++) {
            if (words[i].equals(words[i + 1])) {

                int j = i + 1;

                while (j < words.length && words[j].equals(words[i])) {
                    j++;
                }

                result.append(repeat(words[i], j - i));

                i = j - 1;

                if (i < words.length - 1) {
                    result.append(", ");
                }
            }
        }

        return result.toString();
    }


}
