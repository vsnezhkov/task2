package com.slava.upwork;

import com.slava.upwork.service.WordFilterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class App implements CommandLineRunner {

    @Value("${defaultPhrase}")
    private String defaultPhrase;

    @Autowired
    private WordFilterService wordFilterService;

    @Override
    public void run(String... args) {

        String result = wordFilterService.getSequencesString(defaultPhrase);

        System.out.println(result);
    }

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(App.class);
        app.setBannerMode(Banner.Mode.OFF);
        app.run(args);
    }
}
