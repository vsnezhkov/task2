package com.slava.upwork;

import com.slava.upwork.service.WordFilterService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = App.class)
public class AppTest {

    @Autowired
    private WordFilterService wordFilterService;

    @Test
    public void testPhrase() {
        assertEquals("two two, go go go, string string, is is, end end",
                wordFilterService.getSequencesString("one two two three go go go big small big how sample test text to do it default returns string string is is in seconds since what is the end end"));
    }

    @Test
    public void testSingleWord() {
        assertEquals("", wordFilterService.getSequencesString("one"));
    }

    @Test
    public void testTwoWords() {
        assertEquals("one one", wordFilterService.getSequencesString("one one"));
    }

    @Test
    public void testThreeWords() {
        assertEquals("one one", wordFilterService.getSequencesString("one one two"));
    }

    @Test
    public void testSpaces() {
        assertEquals("one one", wordFilterService.getSequencesString("one          one"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testBlank() {
        wordFilterService.getSequencesString("");
    }
}
